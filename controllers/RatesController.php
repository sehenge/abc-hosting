<?php
session_start();
require_once "../Models/Database.php";
$db = new Database();
if ($_GET['action']) {
    $action = $_GET['action'];
    switch ($action) {
        case "rate":
            $response = $db->setProductRating($_GET['product'], $_GET['rate']);
            $_SESSION['rates'][$_GET['product']] = $_GET['rate'];
            echo json_encode($response);
    }
}
