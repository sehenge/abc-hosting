<?php
session_start();
require_once "../Models/Database.php";
require_once "../Models/Cart.php";
$db = new Database();
$cart = new Cart();
if ($_GET['action']) {
    $action = $_GET['action'];
    switch ($action) {
        case "add":
            $productById = $db->getProductById($_GET['id']);
            $cart->addToCart($productById);

            header("Location: " . '/cart.php');
            break;
        case "remove":
            $cart->removeItem($_GET['id']);

            header("Location: " . '/cart.php');
            break;
        case "empty":
            unset($_SESSION["cart_item"]);
            header("Location: " . '/cart.php');
            break;
        case "pay":
            if ($_SESSION['balance'] < $_POST['total']) {
                header("Location: " . '/cart.php?error=nem');
                break;
            } else {
                $_SESSION['balance'] -= $_POST['total'];
                unset($_SESSION["cart_item"]);
                header("Location: " . '/cart.php?success=paid');
                break;
            }
    }
}
