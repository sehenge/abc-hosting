<?php

include_once "header.phtml";
require_once "Models/Database.php";
$db = new Database();
$product_array = $db->getAllProducts();
if (isset($_GET['msg']) && $_GET['msg'] == 'empty') {
    $msg = ['type' => 'info', 'msg' => 'Your cart is empty.'];
} else if (isset($_GET['msg']) && $_GET['msg'] !== 'empty') {
    $msg = ['type' => $_GET['type'], 'msg' => $_GET['msg']];
}
?>

<p class="font-italic text-muted mb-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
    incididunt.</p>
<?php if (isset($msg)) : ?>
    <div class="alert alert-<?php echo $msg['type']; ?>">
        <?php echo $msg['msg']; ?>
    </div><br/>
<?php endif; ?>
<div class="row pb-5 mb-4">
    <?php
    if (!empty($product_array)) {
        foreach ($product_array as $product) {
            $product['rating'] = number_format($product['rating'], 2) ?? 0;
            ?>
            <div class="col-lg-3 col-md-6 mb-4 mb-lg-0">
                <form method="post" action="controllers/CartController.php?action=add&id=<?php echo $product["id"]; ?>">
                    <div class="card rounded shadow-sm border-0">
                        <div class="card-body p-4">
                            <a href="<?php echo '/product.php?id=' . $product['id']; ?>" class="text-dark">
                                <div class="img-wr mb-3">
                                    <img
                                            src="<?php echo $product["image"]; ?>" alt="<?php echo $product["name"]; ?>"
                                            class="img-fluid d-block mx-auto mb-3 card-img-top">
                                </div>
                                <h5><?php echo $product["name"]; ?></h5>
                            </a>
                            <p class="small text-muted font-italic">Lorem ipsum dolor sit amet, consectetur adipisicing
                                elit.</p>
                            <div class="product-rating">
                            <span class="rate-badge badge badge-success" style="margin-top: 2px;">
                                <i class="fa fa-star"></i>
                                <span><?php echo $product['rating']; ?></span>
                            </span>
                                <span class="rating-review"> with <span
                                            id="ratesCount"><?php echo $product['count']; ?></span> rating(s)</span>
                            </div>
                            <br/>
                            <div class="product-price">
                                <strong><?php echo "$" . $product["price"] . '/' . $product['unit_measure']; ?></strong>
                            </div>
                            <div class="cart-action">
                                <div class="input-group mb-3">
                                    <input type="number" class="form-control"
                                           placeholder=""
                                           aria-describedby="basic-addon2"
                                           name="quantity"
                                           pattern="[0-9]*">
                                    <div class="input-group-append">
                                        <button type="submit" value="Add to Cart"
                                                class="btnAddAction btn btn-primary pr-4 pl-4">Add to card
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        <?php }
    }
    ?>
</div>


</div>
</body>
