<?php

$id = $_GET['id'] ?? header('Location: /');
if ($id == '') header('Location: /');
require_once 'Models/Database.php';
$db = new Database();
$product = $db->getProductById($id) ?? header('Location: /');
$rating = $db->getProductRating($product['id']);

include_once "header.phtml";
?>
<form method="post" action="/controllers/CartController.php?action=add&id=<?php echo $product["id"]; ?>">
    <div class="row">
        <div class="col-lg-4 order-lg-2 order-1">
            <div class="image_selected"><img src="<?php echo $product['image'] ?>"
                                             alt="<?php echo $product['name'] ?>"></div>
        </div>
        <div class="col-lg-6 order-3">
            <div class="product_description">
                <nav>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/">Products</a></li>
                        <!--                            <li class="breadcrumb-item"><a href="/">Products</a></li>-->
                        <li class="breadcrumb-item active"><?php echo $product['name'] ?></li>
                    </ol>
                </nav>
                <div class="product_name"><?php echo $product['name'] ?></div>

                <div class="product-rating">
                    <div id="rateYo" style="padding: 0; display: inline-block;"></div>
                    <span class="rate-badge badge badge-success" style="margin-top: 2px;">
                                <i class="fa fa-star"></i>
                                <span><?php echo $rating['rating']; ?></span>
                            </span>
                    <span class="rating-review"> with <span
                                id="ratesCount"><?php echo $rating['count']; ?></span> rating(s)</span>
                </div>
                <div><span class="product_price">$<?php echo $product['price'] ?></span></div>
                <hr class="singleline">
                <div><span class="product_info"><span>Lorem ipsum dolor sit amet, <br/> consectetur adipisicing elit, <br/>sed do eiusmod tempor incididunt.
                </div>

                <hr class="singleline">
                <div class="order_info d-flex flex-row">
                    <form action="#">
                </div>
                <div class="cart-action col-lg-5 pl-0">
                    <div class="input-group mb-3">
                        <input type="number" class="form-control"
                               placeholder=""
                               name="quantity"
                               pattern="[0-9]*">
                        <div class="input-group-append">
                            <button type="submit" value="Add to Cart" class="btnAddAction btn btn-primary">Add to card
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
</div>

<script>
    $(function () {
        var $j = jQuery.noConflict();
        let rateYo = $j("#rateYo").rateYo({
            rating: <?php echo $rating['rating'];?>,
            starWidth: "20px",
            readOnly: "<?php echo $readonly = $_SESSION['rates'][$product['id']] ?? '';?>",
            onSet: function (rating, rateYoInstance) {
                alert("You rated it: " + rating);
                $j.get("/controllers/RatesController.php?action=rate&product=" + <?php echo $product['id']; ?> +"&rate=" + rating, function (data) {
                    // console.log(data);
                    $j("#ratesCount").text(JSON.parse(data).count);
                    rateYo.rateYo("option", "readOnly", "true");
                    rateYo.rateYo("option", "rating", JSON.parse(data).rating);
                    $j(".rate-badge").html("<i class=\"fa fa-star\"></i> <span>" + JSON.parse(data).rating + '</span>');
                });
            }
        });
    });
</script>

