<?php
ob_start();
include_once "header.phtml";
?>
<div id="shopping-cart">
    <?php
    if (isset($_GET['error']) && $_GET['error'] == 'nem') {
        $msg = ['type' => 'danger', 'msg' => 'Not enough money.'];
    } else if (isset($_GET['success']) && $_GET['success'] == 'paid') {
        $msg = ['type' => 'success', 'msg' => 'Your payment has been successfully processed.'];
    }
    if (isset($_SESSION["cart_item"])) {
        $total_quantity = 0;
        $total_price = 0;
        $subtotal = 0;
        ?>

        <div class="row">
            <div class="col-lg-12 p-5 bg-white rounded shadow-sm mb-5">

                <?php if (isset($msg)) : ?>
                    <div class="alert alert-<?php echo $msg['type']; ?>">
                        <?php echo $msg['msg']; ?>
                    </div><br/>
                <?php endif; ?>
                <h3 class="txt-heading">Shopping Cart</h3><br/>
                <!-- Shopping cart table -->
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col" class="bg-light">
                                <div class="p-2 px-3 text-uppercase">Product</div>
                            </th>
                            <th scope="col" class="bg-light">
                                <div class="py-2 text-uppercase">Unit Price</div>
                            </th>
                            <th scope="col" class="bg-light">
                                <div class="py-2 text-uppercase">Quantity</div>
                            </th>
                            <th scope="col" class="bg-light">
                                <div class="py-2 text-uppercase">Price</div>
                            </th>
                            <th scope="col" class="bg-light">
                                <div class="py-2 text-uppercase">Remove</div>
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($_SESSION["cart_item"] as $item) :
                            $item_price = $item["quantity"] * $item["price"];
                            $total_price += $item_price;
                            $subtotal += $item_price;
                            ?>
                            <tr>
                                <th scope="row">
                                    <div class="p-2">
                                        <img src="<?php echo $item["image"]; ?>" alt="" width="70"
                                             class="img-fluid rounded shadow-sm">
                                        <div class="ml-3 d-inline-block align-middle">
                                            <h5 class="mb-0"><a href="/product.php?id=<?php echo $item["id"] ?>"
                                                                class="text-dark d-inline-block align-middle"><?php echo $item["name"]; ?></a>
                                            </h5>
                                        </div>
                                    </div>
                                </th>
                                <td class="align-middle"><strong><?php echo "$ " . $item["price"]; ?></strong>
                                </td>
                                <td class="align-middle"><strong><?php echo $item["quantity"]; ?></strong></td>
                                <td class="align-middle">
                                    <strong><?php echo '$' . number_format($item_price, 2); ?></strong></td>
                                <td class="align-middle"><a
                                            href="/controllers/CartController.php?action=remove&id=<?php echo $item["id"]; ?>"
                                            class="text-dark"><i class="fa fa-trash"></i></a></td>
                            </tr>
                        <?php endforeach; ?>
                        <?php echo '<script>let total =' . $total_price . '; let subtotal = ' . $subtotal . '; </script>'; ?>
                        </tbody>
                        <tfoot>
                        <tr>
                            <td colspan="5">
                                <a id="btnEmpty" href="/controllers/CartController.php?action=empty" style="float:right;"
                                   class="mt-4">
                                    <button class="btn btn-warning">Empty Cart</button>
                                </a>
                            </td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
        <form class="was-validated" action="/controllers/CartController.php?action=pay" method="POST">
            <div class="row py-5 p-4 bg-white rounded shadow-sm">
                <div class="col-lg-6">
                    <div class="bg-light rounded-pill px-4 py-3 text-uppercase font-weight-bold">Shipping method</div>
                    <div class="p-4">
                        <div class="custom-control custom-radio">
                            <input class="custom-control-input" type="radio" name="shipping" id="shipping1"
                                   value="pickup" required>
                            <label class="custom-control-label" for="shipping1">
                                Pick up
                            </label>
                        </div>
                        <div class="custom-control custom-radio">
                            <input class="custom-control-input" type="radio" name="shipping" id="shipping2"
                                   value="ups" required>
                            <label class="custom-control-label" for="shipping2">
                                UPS (+$5)
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="bg-light rounded-pill px-4 py-3 text-uppercase font-weight-bold">Order summary</div>
                    <div class="p-4">
                        <p class="font-italic mb-4">Shipping and additional costs are calculated based on values you
                            have
                            entered.</p>
                        <ul class="list-unstyled mb-4">
                            <li class="d-flex justify-content-between py-3 border-bottom">
                                <strong class="text-muted">Order
                                    Subtotal </strong>
                                <strong>$<?php echo $subtotal; ?></strong>
                            </li>
                            <li class="d-flex justify-content-between py-3 border-bottom">
                                <strong class="text-muted">Shipping
                                    and handling</strong>
                                <strong class="shipping-cost">$0.00</strong>
                            </li>
                            <li class="d-flex justify-content-between py-3 border-bottom"><strong
                                        class="text-muted">Total</strong>
                                <h5 class="font-weight-bold">$<span id="total"><?php echo $total_price; ?></span></h5>
                                <input type="hidden" name="total" value="0"/>
                            </li>
                        </ul>
                        <button type="submit" class="btn btn-dark rounded-pill py-2 btn-block">Pay</button>
                    </div>
                </div>
            </div>
        </form>
    <?php } else {
        ?>
<!--        <div class="no-records">Your Cart is Empty</div>-->
        <?php
        if ($msg) {
            header("Location: " . '/?type=' . $msg['type'] . '&msg=' . $msg['msg']);
        } else {
            header("Location: " . '/?type=info&msg=empty');
        }
    }
    ?>
