<?php

/**
 * Class Cart
 */
class Cart
{
    /**
     * @param $productById
     */
    public function addToCart($productById): void
    {
        if (!empty($_POST["quantity"])) { //todo: observe if qty is int not string later
            $itemArray =
                [$productById["id"] =>
                     ['name'     => $productById["name"],
                      'id'       => (int)$productById["id"],
                      'quantity' => (int)$_POST["quantity"],
                      'price'    => $productById["price"],
                      'image'    => $productById["image"]
                     ]
                ];

            if (!empty($_SESSION["cart_item"])) {
                if (in_array($productById["id"], array_keys($_SESSION["cart_item"]))) {
                    foreach ($_SESSION["cart_item"] as $k => $v) {
                        if ($productById["id"] == $k) {
                            if (empty($_SESSION["cart_item"][$k]["quantity"])) {
                                $_SESSION["cart_item"][$k]["quantity"] = 0;
                            }
                            $_SESSION["cart_item"][$k]["quantity"] += $_POST["quantity"];
                        }
                    }
                } else {
                    $flag = false;
                    foreach ($_SESSION["cart_item"] as $k => $item) {
                        if ($k == $_GET['id']) {
                            $_SESSION["cart_item"][$_GET['id']]["quantity"] += $itemArray[$_GET['id']]["quantity"];
                            $flag = true;
                        }
                    }
                    if (!$flag) {
                        $_SESSION["cart_item"][$_GET['id']] = $itemArray[$_GET['id']];
                    }
                }
            } else {
                $_SESSION["cart_item"][$productById["id"]] = $itemArray[$productById["id"]];
            }
        }
    }

    /**
     * @param $id
     */
    public function removeItem($id): void
    {
        if (!empty($_SESSION["cart_item"])) {
            foreach ($_SESSION["cart_item"] as $k => $v) {
                if ($id == $v['id']) //todo: double check if works!
                    unset($_SESSION["cart_item"][$k]);
                if (empty($_SESSION["cart_item"]))
                    unset($_SESSION["cart_item"]);
            }
        }
    }
}
