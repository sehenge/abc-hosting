<?php

/**
 * Class Database
 */
class Database
{
    protected $handle;

    private $host;
    private $username;
    private $password;
    private $database;

    public function __construct()
    {
        $this->host = "localhost";
        $this->username = "id11891026_abctest";
        $this->password = "abctest";
        $this->database = "id11891026_abctest";

        $this->handle = new mysqli($this->host, $this->username, $this->password, $this->database);
    }
/**
    public function getUsers()
    {
        $sql = "SELECT id, username, password FROM users";
        $result = $this->handle->query($sql);
    }


    public function registration($username, $password)
    {
        $sql = "INSERT INTO users (username, password) VALUES ('{$username}', '{$password}')";
        if (!$result = $this->handle->query($sql)) {
            $result = mysqli_error($this->handle);
        }

        return $result;
    }

    public function auth($user, $pass)
    {
        $sql = "SELECT DISTINCT * FROM users WHERE username LIKE '{$user}' AND password LIKE '{$pass}'";
        $result = $this->handle->query($sql);

        if (!$result) {
        } //TODO: change this

        return $result;
    }
**/

    /**
     * @param $id
     *
     * @return bool|mysqli_result|string[]|null
     */
    public function getProductById($id)
    {
        $sql = "SELECT * FROM products WHERE id = $id";
        $result = $this->handle->query($sql);
        $result = mysqli_fetch_assoc($result);

        return $result;
    }

    /**
     * @return bool|mysqli_result
     */
    public function getAllProducts()
    {
        $sql = "SELECT products.*, AVG(product_rating.rating) as rating, COUNT(product_rating.rating) as count
                FROM products 
                LEFT JOIN product_rating 
                ON products.id = product_rating.product_id
                GROUP BY products.id";

        $result = $this->handle->query($sql);

        return $result;
    }

    /**
     * @param $id
     *
     * @return int|string[]|null
     */
    public function getProductRating($id)
    {
        $sql = "SELECT AVG(rating) as rating, COUNT(rating) as count FROM product_rating WHERE product_id = {$id}";
        $result = mysqli_fetch_assoc($this->handle->query($sql));
        $rating = $result ?? 0;
        $rating['rating'] = number_format($rating['rating'], 2);

        return $rating;
    }

    /**
     * @param $id
     * @param $rate
     *
     * @return int|string[]|null
     */
    public function setProductRating($id, $rate)
    {
        $sql = "INSERT INTO product_rating (product_id, rating) VALUES ({$id}, {$rate})";
        $this->handle->query($sql);

        $result = $this->getProductRating($id);

        return $result;
    }
}
