$(document).ready(function () {
    $("input:radio[name=shipping]").change(function () {
        if (this.value == 'pickup') {
            total = subtotal;
            $(".shipping-cost").text('$0.00');
            $('#total').text(total);
        } else if (this.value == 'ups') {
            total = subtotal + 5;
            $(".shipping-cost").text('$5.00');
            $('#total').text(total);
        }
        $("input[name=total]").val(total);
    });
});
